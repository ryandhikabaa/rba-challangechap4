    let count = 0;
    let clickedPart;
    const cardbg = "#c4c4c4";
    const batuCom = document.getElementById("batu-com");
    const kertasCom = document.getElementById("kertas-com");
    const guntingCom = document.getElementById("gunting-com");
    const versus = document.getElementById("versus");
    const changeable = document.getElementById("changeable");
    const draw = '<h3 class="position-absolute top-50 start-50 translate-middle">Draw!</h3>';
    const p1w = '<h3 class="position-absolute top-50 start-50 translate-middle">Player 1 Win!</h3>';
    const comw = '<h3 class="position-absolute top-50 start-50 translate-middle">Com Win!</h3>';
    const drawColor = "#035B0C";
    const winColor = "#4C9654";

    function toChangeCard() {
        versus.style.display = "none";
        changeable.style.display = "block";
    }

    function randomNumb() {
        //3 and 1 only for baffle or limit       
        return Math.floor(Math.random() * 3) + 1;
    }

    //get the clicked element to change background
    function changeBackground(choice, rand) {
        clickedPart = document.getElementById(choice);
        clickedPart.style.backgroundColor = cardbg;
        switch (rand) {
            case 1:
                batuCom.style.backgroundColor = cardbg;
                break;
            case 2:
                kertasCom.style.backgroundColor = cardbg;
                break;
            case 3:
                guntingCom.style.backgroundColor = cardbg;
                break;
        }
    }

    // get return number from choice
    function choiceToInt(choice) {
        switch (choice) {
            case "batu-p1":
                return 1;
                break;
            case "kertas-p1":
                return 2;
                break;
            case "gunting-p1":
                return 3;
                break;
        }
    }

    //to see who is the winner, from choice clicked and random number
    function whoWin(choice, rand) {
        let returnable;
        choice = choiceToInt(choice);
        if (choice === rand) returnable = 3;
        else if (choice === 1) rand === 2 ? returnable = 2 : returnable = 1;
        else if (choice === 2) rand === 3 ? returnable = 2 : returnable = 1;
        else if (choice === 3) rand === 1 ? returnable = 2 : returnable = 1;
        return returnable;
    }

    //change text result after clicked
    function afterMathChange(aftermath) {
        switch (aftermath) {
            case 1:
                changeable.innerHTML = p1w;
                changeable.style.backgroundColor = winColor;
                break;
            case 2:
                changeable.innerHTML = comw;
                changeable.style.backgroundColor = winColor;
                break;
            case 3:
                changeable.innerHTML = draw;
                changeable.style.backgroundColor = drawColor;
                break;
        }
    }

    //main function from the clicked, This is for only 1 match before refresh because it only runs when the count is 0
    function clicked(choice) {
        if (count == 0) {
            count++;
            toChangeCard();
            let rand = randomNumb();
            changeBackground(choice, rand);
            let aftermath = whoWin(choice, rand);
            console.log(aftermath);
            afterMathChange(aftermath);
        }
    }

    //to set initial conditions
    function refresh() {
        count = 0;
        clickedPart.style.backgroundColor = "";
        batuCom.style.backgroundColor = "";
        kertasCom.style.backgroundColor = "";
        guntingCom.style.backgroundColor = "";
        changeable.style.backgroundColor = "";
        changeable.style.display = "none";
        versus.style.display = "block";
    }